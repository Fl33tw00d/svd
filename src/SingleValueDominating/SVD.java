/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SingleValueDominating;

import java.util.Random;

/**
 *
 * @author vhe17qgu
 */
public class SVD {

    //Quadratic approach using nested for loops
    private static void SVDPT1(int[] A) {
        int maxVal = 0;
        int index = -1;

        for (int i = 0; i < A.length; i++) {
            int counter = 0;
            for (int j = 0; j < A.length; j++) {

                if (A[i] == A[j]) {
                    counter++;
                }

                if (counter > maxVal) {
                    maxVal = counter;

                    index = i;
                }
            }

        }

        if (maxVal > (A.length / 2)) {
            System.out.println("The SVD of the array is: "
                    + A[index]);
        } else {
            System.out.println("No SVD found.");
        }

    }

    //Merge sort approach
    private static void SVDPT2(int[] A) {
        int counter = 0;
        double median = 0;
        //If the array  is length 1 then we know that the SVD is the only element
        if (A.length == 1) {
            System.out.println("The SVD of the Array is: " + A[0]);
        }

        sort(A, 0, A.length - 1);
        //Finding median value
        if (A.length % 2 == 0) {
            median = (A[A.length / 2 - 1] + A[A.length / 2]) / 2.0;
        } else {
            median = A[A.length / 2];
        }

        //Ensuring that the median actually is the SVD
        for (int element : A) {
            if (element == median) {
                counter++;
            }
        }
        
        
        if (counter > (A.length / 2)) {
            System.out.println("The SVD of the array is: "
                    + median);
        } else {
            System.out.println("No SVD found.");
        }

    }

    //Sort function that recursively calls itself
   static void sort(int arr[], int Left, int Right) 
    { 
        if (Left < Right) 
        { 
            // Find the middle point 
            int Mid = (Left+Right)/2; 
  
            // Sort first and second halves 
            sort(arr, Left, Mid); 
            sort(arr , Mid+1, Right); 
  
            // Merge the sorted halves 
            merge(arr, Left, Mid, Right); 
        } 
    } 

   //Merge Function
   static void merge(int arr[], int Left, int Mid, int Right) 
    { 
        // Size of 2 sub arrays
        int Size1 = Mid - Left + 1; 
        int Size2 = Right - Mid; 
  
        //Creating the 2 sub arrays
        int LTemp[] = new int [Size1]; 
        int RTemp[] = new int [Size2]; 
  
        //Copying info into the sub arrays
        for (int i=0; i<Size1; ++i) 
            LTemp[i] = arr[Left + i]; 
        for (int j=0; j<Size2; ++j) 
            RTemp[j] = arr[Mid + 1+ j]; 
  
  
        // Initial indexes of first and second subarrays 
        int i = 0, j = 0; 
  
        // Initial index of merged subarry array 
        int k = Left; 
        while (i < Size1 && j < Size2) 
        { 
            if (LTemp[i] <= RTemp[j]) 
            { 
                arr[k] = LTemp[i]; 
                i++; 
            } 
            else
            { 
                arr[k] = RTemp[j]; 
                j++; 
            } 
            k++; 
        } 
  
        //Copy remaining elements into array
        while (i < Size1) 
        { 
            arr[k] = LTemp[i]; 
            i++; 
            k++; 
        } 
  
        //Copying remaining elements into array
        while (j < Size2) 
        { 
            arr[k] = RTemp[j]; 
            j++; 
            k++; 
        } 
    } 

   //Linear approach using the Boyer-Moore voting algorithm
    private static void SVDPT3(int[] A) {
        int svdIndex = 0;
        int counter = 1;
        int checker = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[svdIndex] == A[i]) {
                counter++;
            } else {
                counter--;
            }

            if (counter == 0) {
                svdIndex = i;
                counter = 1;
            }
        }

        //Checking if the candidate chosen is the SVD
        for (int i = 0; i < A.length; i++) {
            if (A[i] == A[svdIndex]) {
                checker++;
            }
        }

        if (checker > (A.length / 2)) {
            System.out.println("The SVD of the array is: "
                        + A[svdIndex]);
        } else {
            System.out.println("No SVD found.");
        }
    }

    public static void main(String[] args) {

        //Testing Variables
        int[] testArray = new int[100000];
        int lowerB = 0;
        int upperB = 50;
        int increment = 1;
        Random r = new Random();

        while(lowerB < upperB){
        //Method for generating an array that will always contain an SVD        
        for (int i = 0; i < testArray.length; i++) {
            //By using every even number + a prime factor, we will be sure
            //that the desired value is a majority element and the others
            //are random numbers between 0 and 100
            if (i % 2 == 0 || i % 17 == 0) {
                testArray[i] = 28;
                //Testing
                // System.out.print(testArray[i] + ", ");
            } else {
                testArray[i] = r.nextInt(100);
                //Testing
                // System.out.print(testArray[i] + ", ");
            }
        }
        
        //Timing Experiment
        long startTime = System.nanoTime();
        SVDPT2(testArray);
        long endTime = System.nanoTime();
        long elapsedTime = endTime - startTime;
        System.out.println("Time Elapsed (Microseconds): " + (elapsedTime / 1000));
        lowerB += increment;
        }



    }

}
